<?php
/**
 * Plugin Name:       Customers
 * Plugin URI:        logicsbuffer.com/
 * Description:       Customers Management For TantiOptics [customers]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       customers-mng
 * Domain Path:       /languages
 */

	add_action( 'init', 'pallettool' );
	function pallettool() {
		add_shortcode( 'customers', 'pallet_tool_single' );
		add_action( 'wp_enqueue_scripts', 'chop_tool_script' );
	}

	function chop_tool_script() {		        
		wp_enqueue_script( 'lbpt_main_script', plugins_url().'/customers/js/script.js',array(),time());	
		wp_enqueue_style( 'lbpt_main_style', plugins_url().'/customers/css/style.css',array(),time());
	}

function pallet_tool_single($atts) {
	
	//Get current Product id for cart details
		global $product;
		global $woocommerce;
		global $post;
		//$post->ID;
		$product_id = $post->ID;

	if(isset($_POST['save_size'])){
			
		// //$total_qty = $_POST['rt_qty'];			 
		// $quantity = 1;			 
		
		// $rt_total_price = $_REQUEST['total_price'];
		// $nmber_of_pallets = $_REQUEST['nmber_of_pallets'];
		// echo $rt_total_price;
		// update_post_meta($product_id, 'total_price', $rt_total_price);
		// update_post_meta($product_id, 'nmber_of_pallets', $nmber_of_pallets); 

		//Ajax Code
		//$h = $_REQUEST['lb_cropperh'];
		$phone_num = $_REQUEST['phone_num'];
	    //echo $phone_num;
	    $results = array();

	    $phone_num = $phone_num;
 
		$my_args = array( 
			'post_type'  => 'customer',
			"s" => $phone_num,
		); 
		 
		$custom_query = new WP_Query( $my_args );
		 
		if ( $custom_query->have_posts() ) {
		    while ( $custom_query->have_posts() ) {
		        $custom_query->the_post();
		 	
		 		$current_post_id = get_the_ID();
				//echo $current_post_id;
				// the_field('accno');
				// echo "<br>";
				// the_field('customer_name');
				// echo "<br>";
				// the_field('add');
				// echo "<br>";
				// the_field('add2'); 
				
		 		$results[] = get_the_title();
		 		$accno = get_field( "accno", $current_post_id);		 
		 		$customer_name = get_field( "customer_name", $current_post_id);		 
		 		$add = get_field( "add", $current_post_id);		 
		 		$add2 = get_field( "add2", $current_post_id);		 
		 		$invno = get_field( "invno", $current_post_id);		 
		 		$date = get_field( "date", $current_post_id);		 
		 		$deldate = get_field( "deldate", $current_post_id);		 
		 		$cur_date = get_field( "cur_date", $current_post_id);		 
		 		$fromacc = get_field( "fromacc", $current_post_id);		 
		 		$stktr = get_field( "stktr", $current_post_id);		 
		 		$sph_r_ = get_field( "sph_r_", $current_post_id);		 
		 		$sph_r_2_ = get_field( "sph_r_2_", $current_post_id);		 
		 		$sph_r_3 = get_field( "sph_r_3", $current_post_id);		 
		 		$sph_l_ = get_field( "sph_l_", $current_post_id);		 
		 		$sph_l_2 = get_field( "sph_l_2", $current_post_id);		 
		 		$sph_l_3_ = get_field( "sph_l_3_", $current_post_id);		 
		 		$cyl_r = get_field( "cyl_r", $current_post_id);		 
		 		$cyl_r_2_ = get_field( "cyl_r_2_", $current_post_id);		 
		 		$cyl_r_3 = get_field( "cyl_r_3", $current_post_id);		 
		 		$cyl_l = get_field( "cyl_l", $current_post_id);		 
		 		$cyl_l_2 = get_field( "cyl_l_2", $current_post_id);		 
		 		$cyl_l_3_ = get_field( "cyl_l_3_", $current_post_id);		 
		 		$axis_r = get_field( "axis_r", $current_post_id);		 
		 		$axis_r_2_ = get_field( "axis_r_2_", $current_post_id);		 
		 		$axis_r_3 = get_field( "axis_r_3", $current_post_id);		 
		 		$axis_l = get_field( "axis_l", $current_post_id);		 
		 		$axis_l_2 = get_field( "axis_l_2", $current_post_id);		 
		 		$axis_l_3 = get_field( "axis_l_3", $current_post_id);		 
		 		$prism_r_ = get_field( "prism_r_", $current_post_id);		 
		 		$prism_l = get_field( "prism_l", $current_post_id);		 
		 		$prtype_l = get_field( "prtype_l", $current_post_id);		 
		 		$prtype_r = get_field( "prtype_r", $current_post_id);		 
		 		$prtype_r2 = get_field( "prtype_r2", $current_post_id);		 
		 		$prtype_r3 = get_field( "prtype_r3", $current_post_id);		 
		 		$addition = get_field( "addition", $current_post_id);		 
		 		$addition2 = get_field( "addition2", $current_post_id);		 
		 		$addition3_ = get_field( "addition3_", $current_post_id);		 
		 		$right_ = get_field( "right_", $current_post_id);		 
		 		$left_ = get_field( "left_", $current_post_id);		 
		 		$near_far_ = get_field( "near_far_", $current_post_id);		 
		 		$near_far2_ = get_field( "near_far_", $current_post_id);		 
		 		$near_far3 = get_field( "near_far3", $current_post_id);		 
		 		$n_f_bi = get_field( "n_f_bi", $current_post_id);		 
		 		$n_f_bi2_ = get_field( "n_f_bi2_", $current_post_id);		 
		 		$n_f_bi3 = get_field( "n_f_bi3", $current_post_id);		 
		 		$cl_nfbi = get_field( "cl_nfbi", $current_post_id);		 
		 		$l_type_r_ = get_field( "l_type_r_", $current_post_id);		 
		 		$l_type_r2 = get_field( "l_type_r2", $current_post_id);		 
		 		$l_type_r3 = get_field( "l_type_r3", $current_post_id);		 
		 		$l_type_l_ = get_field( "l_type_l_", $current_post_id);		 
		 		$l_type_l2_ = get_field( "l_type_l2_", $current_post_id);		 
		 		$l_type_l3 = get_field( "l_type_l3", $current_post_id);		 
		 		$l_rate = get_field( "l_rate", $current_post_id);		 
		 		$l_link_r_ = get_field( "l_link_r_", $current_post_id);		 
		 		$l_link_r2 = get_field( "l_link_r2", $current_post_id);		 
		 		$l_link_r3_ = get_field( "l_link_r3_", $current_post_id);		 
		 		$l_link_l_ = get_field( "l_link_l_", $current_post_id);		 
		 		$l_link_l2 = get_field( "l_link_l2", $current_post_id);		 
		 		$l_link_l3 = get_field( "l_link_l3", $current_post_id);		 
		 		$cl_link_r_ = get_field( "cl_link_r_", $current_post_id);		 
		 		$cl_link_l_ = get_field( "cl_link_l_", $current_post_id);		 
		 		$l_qty_r = get_field( "l_qty_r", $current_post_id);		 
		 		$l_qty_r2 = get_field( "l_qty_r2", $current_post_id);		 
		 		$l_qty_r3 = get_field( "l_qty_r3", $current_post_id);		 
		 		$l_qty_l_ = get_field( "l_qty_l_", $current_post_id);		 
		 		$l_qty_l2 = get_field( "l_qty_l2", $current_post_id);		 
		 		$l_qty_l3_ = get_field( "l_qty_l3_", $current_post_id);		 
		 		$lamt1 = get_field( "lamt1", $current_post_id);		 
		 		$lamt2 = get_field( "lamt2", $current_post_id);		 
		 		$lamt3 = get_field( "lamt3", $current_post_id);		 
		 		$clamt_ = get_field( "clamt_", $current_post_id);		 
		 		$cl_sph_r = get_field( "cl_sph_r", $current_post_id);		 
		 		$cl_sph_l_ = get_field( "cl_sph_l_", $current_post_id);		 
		 		$cl_cyl_r_ = get_field( "cl_cyl_r_", $current_post_id);		 
		 		$cl_cyl_l_ = get_field( "cl_cyl_l_", $current_post_id);		 
		 		$cl_axis_r_ = get_field( "cl_axis_r_", $current_post_id);		 
		 		$cl_axis_l = get_field( "cl_axis_l", $current_post_id);		 
		 		$cl_type_r_ = get_field( "cl_type_r_", $current_post_id);		 
		 		$cl_type_l_ = get_field( "cl_type_l_", $current_post_id);		 
		 		$cl_adtn = get_field( "cl_adtn", $current_post_id);		 
		 		$cl_dia_r_ = get_field( "cl_dia_r_", $current_post_id);		 
		 		$cl_dia_l = get_field( "cl_dia_l", $current_post_id);		 
		 		$cl_bcurv_r = get_field( "cl_bcurv_r", $current_post_id);		 
		 		$cl_bcurv_l = get_field( "cl_bcurv_l", $current_post_id);		 
		 		$cl_qty_r_ = get_field( "cl_qty_r_", $current_post_id);		 
		 		$cl_qty_l = get_field( "cl_qty_l", $current_post_id);		 
		 		$inv_no_b_ = get_field( "inv_no_b_", $current_post_id);		 
		 		$descript = get_field( "descript", $current_post_id);		 
		 		$brand = get_field( "brand", $current_post_id);		 
		 		$model = get_field( "model", $current_post_id);		 
		 		$color_ = get_field( "color_", $current_post_id);		 
		 		$size = get_field( "size", $current_post_id);		 
		 		$material = get_field( "material", $current_post_id);		 
		 		$code = get_field( "code", $current_post_id);	

		 		// Test Html
		 		?>
		 		<div class="dataheader">
			 		<h2><?php the_field('accno'); ?></h2>
			 		<h2><?php the_field('customer_name'); ?></h2> 
				</div>

				<table style="width:100%">

                     <?php if($add) {?>
					 <tr>
                       <th>Address</th>
                        <td>
							<?php echo $add;?>
						</td>
                     </tr>
					 <?php }?>
					 
					  <?php if($accno) {?>
					  <tr>
                       <th>Account Number</th>
                        <td>
							<?php echo $accno;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 <?php if($customer_name) {?>
					  <tr>
                       <th>Customer Name</th>
                        <td>
							<?php echo $customer_name;?>
						</td>
                     </tr>
					 <?php }?>
					 
					
                     
					 <?php if($add) {?>
					   <tr>
                       <th>Address</th>
                        <td>
							<?php echo $add;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($add2) {?>
					   <tr>
                       <th>Number</th>
                        <td>
							<?php echo $add2;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($invno) {?>
					   <tr>
                       <th>Invoice Number</th>
                        <td>
							<?php echo $invno;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($date) {?>
					   <tr>
                       <th>Date</th>
                        <td>
							<?php echo $date;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($deldate) {?>
					   <tr>
                       <th>Delay Date</th>
                        <td>
							<?php echo $deldate;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 <?php if($cur_date) {?>
					   <tr>
                       <th>Current Date</th>
                        <td>
							<?php echo $cur_date;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 
					 
					 <?php if($code) {?>
					   <tr>
                       <th>Code</th>
                        <td>
							<?php echo $code;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($model) {?>
					   <tr>
                       <th>Model</th>
                        <td>
							<?php echo $model;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($color_) {?>
					   <tr>
                       <th>Color</th>
                        <td>
							<?php echo $color_;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 <?php if($size) {?>
					   <tr>
                       <th>Size</th>
                        <td>
							<?php echo $size;?>
						</td>
                     </tr>
					 <?php }?> 
					 
					 
					 <?php if($material) {?>
					   <tr>
                       <th>Material</th>
                        <td>
							<?php echo $material;?>
						</td>
                     </tr>
					 <?php }?>

			
					 
					 <?php if($fromacc) {?>
					   <tr>
                       <th>fromacc</th>
                        <td>
							<?php echo $fromacc;?>
						</td>
                     </tr>
					 <?php }?>
			
					 
					 <?php if($stktr) {?>
					   <tr>
                       <th>stktr</th>
                        <td>
							<?php echo $stktr;?>
						</td>
                     </tr>
					 <?php }?>

			 
					 <?php if($sph_r_) {?>
					   <tr>
                       <th>sph_r_</th>
                        <td>
							<?php echo $sph_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
			 
					 <?php if($sph_r_2_) {?>
					   <tr>
                       <th>sph_r_2_</th>
                        <td>
							<?php echo $sph_r_2_;?>
						</td>
                     </tr>
					 <?php }?>

			
			 
					 <?php if($sph_r_3) {?>
					   <tr>
                       <th>sph_r_3</th>
                        <td>
							<?php echo $sph_r_3;?>
						</td>
                     </tr>
					 <?php }?>

			
			
			 
					 <?php if($sph_l_) {?>
					   <tr>
                       <th>sph_l_</th>
                        <td>
							<?php echo $sph_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
			
			 
					 <?php if($sph_l_2) {?>
					   <tr>
                       <th>sph_l_2</th>
                        <td>
							<?php echo $sph_l_2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($sph_r_) {?>
					   <tr>
                       <th>sph_r_</th>
                        <td>
							<?php echo $sph_r_;?>
						</td>
                     </tr>
					 <?php }?> 
					
			
			 
					 <?php if($sph_r_2_) {?>
					   <tr>
                       <th>sph_r_2_</th>
                        <td>
							<?php echo $sph_r_2_;?>
						</td>
                     </tr>
					 <?php }?> 
					 
					 
					 
					 <?php if($sph_r_3) {?>
					   <tr>
                       <th>sph_r_3</th>
                        <td>
							<?php echo $sph_r_3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($sph_l_) {?>
					   <tr>
                       <th>sph_l_</th>
                        <td>
							<?php echo $sph_l_;?>
						</td>
                     </tr>
					 <?php }?>

					
			
			 
					 <?php if($sph_l_2) {?>
					   <tr>
                       <th>sph_l_2</th>
                        <td>
							<?php echo $sph_l_2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($sph_l_3_) {?>
					   <tr>
                       <th>sph_l_3_</th>
                        <td>
							<?php echo $sph_l_3_;?>
						</td>
                     </tr>
					 <?php }?>

					
			
			 
					 <?php if($cyl_r) {?>
					   <tr>
                       <th>cyl_r</th>
                        <td>
							<?php echo $cyl_r;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($cyl_r_2_) {?>
					   <tr>
                       <th>cyl_r_2_</th>
                        <td>
							<?php echo $cyl_r_2_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
					
			
			 
					 <?php if($cyl_r_3) {?>
					   <tr>
                       <th>cyl_r_3</th>
                        <td>
							<?php echo $cyl_r_3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($cyl_l) {?>
					   <tr>
                       <th>cyl_l</th>
                        <td>
							<?php echo $cyl_l;?>
						</td>
                     </tr>
					 <?php }?>

					
			
			 
					 <?php if($cyl_l_2) {?>
					   <tr>
                       <th>cyl_l_2</th>
                        <td>
							<?php echo $cyl_l_2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
			 
					 <?php if($cyl_l_3_) {?>
					   <tr>
                       <th>cyl_l_3_</th>
                        <td>
							<?php echo $cyl_l_3_;?>
						</td>
                     </tr>
					 <?php }?>

                   
			 
					 <?php if($axis_r) {?>
					   <tr>
                       <th>axis_r</th>
                        <td>
							<?php echo $axis_r;?>
						</td>
                     </tr>
					 <?php }?>

                   
			 
					 <?php if($axis_r_2_) {?>
					   <tr>
                       <th>axis_r_2_</th>
                        <td>
							<?php echo $axis_r_2_;?>
						</td>
                     </tr>
					 <?php }?>

			
			 
					 <?php if($axis_r_3) {?>
					   <tr>
                       <th>axis_r_3</th>
                        <td>
							<?php echo $axis_r_3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			 
					 <?php if($axis_l) {?>
					   <tr>
                       <th>axis_l</th>
                        <td>
							<?php echo $axis_l;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			 
					 <?php if($axis_l_2) {?>
					   <tr>
                       <th>axis_l_2</th>
                        <td>
							<?php echo $axis_l_2;?>
						</td>
                     </tr>
					 <?php }?>

					
			 
					 <?php if($axis_l_3) {?>
					   <tr>
                       <th>axis_l_3</th>
                        <td>
							<?php echo $axis_l_3;?>
						</td>
                     </tr>
					 <?php }?>
					
			 
					 <?php if($prism_r_) {?>
					   <tr>
                       <th>prism_r_</th>
                        <td>
							<?php echo $prism_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 
					 <?php if($prism_l) {?>
					   <tr>
                       <th>prism_l</th>
                        <td>
							<?php echo $prism_l;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					  
					 <?php if($prtype_l) {?>
					   <tr>
                       <th>prtype_l</th>
                        <td>
							<?php echo $prtype_l;?>
						</td>
                     </tr>
					 <?php }?>
                   
					  
					 <?php if($prtype_r) {?>
					   <tr>
                       <th>prtype_r</th>
                        <td>
							<?php echo $prtype_r;?>
						</td>
                     </tr>
					 <?php }?>

					  
					 <?php if($prtype_r2) {?>
					   <tr>
                       <th>prtype_r2</th>
                        <td>
							<?php echo $prtype_r2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($prtype_r3) {?>
					   <tr>
                       <th>prtype_r3</th>
                        <td>
							<?php echo $prtype_r3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($addition) {?>
					   <tr>
                       <th>addition</th>
                        <td>
							<?php echo $addition;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($addition2) {?>
					   <tr>
                       <th>addition2</th>
                        <td>
							<?php echo $addition2;?>
						</td>
                     </tr>
					 <?php }?>
					
                   
					 <?php if($addition3_) {?>
					   <tr>
                       <th>addition3_</th>
                        <td>
							<?php echo $addition3_;?>
						</td>
                     </tr>
					 <?php }?>

			
                   
					 <?php if($right_) {?>
					   <tr>
                       <th>right_</th>
                        <td>
							<?php echo $right_;?>
						</td>
                     </tr>
					 <?php }?>

			
                   
					 <?php if($left_) {?>
					   <tr>
                       <th>left_</th>
                        <td>
							<?php echo $left_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
                   
					 <?php if($near_far_) {?>
					   <tr>
                       <th>near_far_</th>
                        <td>
							<?php echo $near_far_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($near_far2_) {?>
					   <tr>
                       <th>near_far2_</th>
                        <td>
							<?php echo $near_far2_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
			
					
					 <?php if($near_far3) {?>
					   <tr>
                       <th>near_far3</th>
                        <td>
							<?php echo $near_far3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
                   
			
					
					 <?php if($n_f_bi) {?>
					   <tr>
                       <th>n_f_bi</th>
                        <td>
							<?php echo $n_f_bi;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
			
					
					 <?php if($n_f_bi2_) {?>
					   <tr>
                       <th>n_f_bi2_</th>
                        <td>
							<?php echo $n_f_bi2_;?>
						</td>
                     </tr>
					 <?php }?>

                   
			
					
					 <?php if($n_f_bi3) {?>
					   <tr>
                       <th>n_f_bi3</th>
                        <td>
							<?php echo $n_f_bi3;?>
						</td>
                     </tr>
					 <?php }?>


					 <?php if($cl_nfbi) {?>
					   <tr>
                       <th>cl_nfbi</th>
                        <td>
							<?php echo $cl_nfbi;?>
						</td>
                     </tr>
					 <?php }?>
			
					
                   
			
					
					 <?php if($l_type_r_) {?>
					   <tr>
                       <th>l_type_r_</th>
                        <td>
							<?php echo $l_type_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($l_type_r2) {?>
					   <tr>
                       <th>l_type_r2</th>
                        <td>
							<?php echo $l_type_r2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($l_type_r3) {?>
					   <tr>
                       <th>l_type_r3</th>
                        <td>
							<?php echo $l_type_r3;?>
						</td>
                     </tr>
					 <?php }?>

			
					

			
					
                   
					 
					
					 <?php if($l_type_l_) {?>
					   <tr>
                       <th>l_type_l_</th>
                        <td>
							<?php echo $l_type_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
					
                   
					 
					
					 <?php if($l_type_l2_) {?>
					   <tr>
                       <th>l_type_l2_</th>
                        <td>
							<?php echo $l_type_l2_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 
					
					 <?php if($l_type_l3) {?>
					   <tr>
                       <th>l_type_l3</th>
                        <td>
							<?php echo $l_type_l3;?>
						</td>
                     </tr>
					 <?php }?>
					 
					
					 <?php if($l_rate) {?>
					   <tr>
                       <th>l_rate</th>
                        <td>
							<?php echo $l_rate;?>
						</td>
                     </tr>
					 <?php }?>
					
					 <?php if($l_link_r_) {?>
					   <tr>
                       <th>l_link_r_</th>
                        <td>
							<?php echo $l_link_r_;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($l_link_r2) {?>
					   <tr>
                       <th>l_link_r2</th>
                        <td>
							<?php echo $l_link_r2;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($l_link_r3_) {?>
					   <tr>
                       <th>l_link_r3_</th>
                        <td>
							<?php echo $l_link_r3_;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($l_link_l_) {?>
					   <tr>
                       <th>l_link_l_</th>
                        <td>
							<?php echo $l_link_l_;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($l_link_l2) {?>
					   <tr>
                       <th>l_link_l2</th>
                        <td>
							<?php echo $l_link_l2;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($l_link_l3) {?>
					   <tr>
                       <th>l_link_l3</th>
                        <td>
							<?php echo $l_link_l3;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($cl_link_r_) {?>
					   <tr>
                       <th>cl_link_r_</th>
                        <td>
							<?php echo $cl_link_r_;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($cl_link_l_) {?>
					   <tr>
                       <th>cl_link_l_</th>
                        <td>
							<?php echo $cl_link_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($l_qty_r) {?>
					   <tr>
                       <th>l_qty_r</th>
                        <td>
							<?php echo $l_qty_r;?>
						</td>
                     </tr>
					 <?php }?>
					
					 <?php if($l_qty_r2) {?>
					   <tr>
                       <th>l_qty_r2</th>
                        <td>
							<?php echo $l_qty_r2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($l_qty_r3) {?>
					   <tr>
                       <th>l_qty_r3</th>
                        <td>
							<?php echo $l_qty_r3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 
					
					 <?php if($l_qty_l_) {?>
					   <tr>
                       <th>l_qty_l_</th>
                        <td>
							<?php echo $l_qty_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					
					 <?php if($l_qty_l2) {?>
					   <tr>
                       <th>l_qty_l2</th>
                        <td>
							<?php echo $l_qty_l2;?>
						</td>
                     </tr>
					 <?php }?>
                   
					
					 <?php if($l_qty_l3_) {?>
					   <tr>
                       <th>l_qty_l3_</th>
                        <td>
							<?php echo $l_qty_l3_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($lamt1) {?>
					   <tr>
                       <th>lamt1</th>
                        <td>
							<?php echo $lamt1;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($lamt2) {?>
					   <tr>
                       <th>lamt2</th>
                        <td>
							<?php echo $lamt2;?>
						</td>
                     </tr>
					 <?php }?>

			
					 <?php if($lamt3) {?>
					   <tr>
                       <th>lamt3</th>
                        <td>
							<?php echo $lamt3;?>
						</td>
                     </tr>
					 <?php }?>

			
					 <?php if($clamt_) {?>
					   <tr>
                       <th>clamt_</th>
                        <td>
							<?php echo $clamt_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($cl_sph_r) {?>
					   <tr>
                       <th>cl_sph_r</th>
                        <td>
							<?php echo $cl_sph_r;?>
						</td>
                     </tr>
					 <?php }?>
	
                   
					 <?php if($cl_sph_l_) {?>
					   <tr>
                       <th>cl_sph_l_</th>
                        <td>
							<?php echo $cl_sph_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					 <?php if($cl_cyl_r_) {?>
					   <tr>
                       <th>cl_cyl_r_</th>
                        <td>
							<?php echo $cl_cyl_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
			
					 <?php if($cl_cyl_l_) {?>
					   <tr>
                       <th>cl_cyl_l_</th>
                        <td>
							<?php echo $cl_cyl_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					 <?php if($cl_axis_r_) {?>
					   <tr>
                       <th>cl_axis_r_</th>
                        <td>
							<?php echo $cl_axis_r_;?>
						</td>
                     </tr>
					 <?php }?>
			
					 <?php if($cl_axis_l) {?>
					   <tr>
                       <th>cl_axis_l</th>
                        <td>
							<?php echo $cl_axis_l;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($cl_type_r_) {?>
					   <tr>
                       <th>cl_type_r_</th>
                        <td>
							<?php echo $cl_type_r_;?>
						</td>
                     </tr>
					 <?php }?>

                   
					 <?php if($cl_type_l_) {?>
					   <tr>
                       <th>cl_type_l_</th>
                        <td>
							<?php echo $cl_type_l_;?>
						</td>
                     </tr>
					 <?php }?>

                   
					 <?php if($cl_adtn) {?>
					   <tr>
                       <th>cl_adtn</th>
                        <td>
							<?php echo $cl_adtn;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($cl_dia_r_) {?>
					   <tr>
                       <th>cl_dia_r_</th>
                        <td>
							<?php echo $cl_dia_r_;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($cl_dia_l) {?>
					   <tr>
                       <th>cl_dia_l</th>
                        <td>
							<?php echo $cl_dia_l;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($cl_bcurv_r) {?>
					   <tr>
                       <th>cl_bcurv_r</th>
                        <td>
							<?php echo $cl_bcurv_r;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($cl_bcurv_l) {?>
					   <tr>
                       <th>cl_bcurv_l</th>
                        <td>
							<?php echo $cl_bcurv_l;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($cl_qty_r_) {?>
					   <tr>
                       <th>cl_qty_r_</th>
                        <td>
							<?php echo $cl_qty_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 
					 <?php if($cl_qty_l) {?>
					   <tr>
                       <th>cl_qty_l</th>
                        <td>
							<?php echo $cl_qty_l;?>
						</td>
                     </tr>
					 <?php }?>

					
					 
					 <?php if($inv_no_b_) {?>
					   <tr>
                       <th>inv_no_b_</th>
                        <td>
							<?php echo $inv_no_b_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 
					 <?php if($descript) {?>
					   <tr>
                       <th>descript</th>
                        <td>
							<?php echo $descript;?>
						</td>
                     </tr>
					 <?php }?>
               </table>
		 		<h2>1.Lense(Prescription History)</h2>

<div class="row">
  <div class="column">
    <table>
	<h4>RIGHT</h4>
      <tr>
        <th>Date</th>
        <th>OrderNO.</th>
        <th>Sph</th>
		<th>Cyl</th>
		<th>Axis</th>
		<th>Prism</th>
		<th>P.Type</th>
		<th>L.Type</th>
		
		
      </tr>
      <tr>
        <td><?php if($date) {  echo $date;  }?></td>
        <td><?php if($inv_no_b_) {  echo $inv_no_b_;  }?></td>
        <td><?php if($sph_r_) {  echo $sph_r_;  }?></td>
		<td><?php if($cyl_r) {  echo $cyl_r;  }?></td>
		<td><?php if($axis_r) {  echo $axis_r;  }?></td>
		<td><?php if($prism_r_) {  echo $prism_r_;  }?></td>	
		<td><?php if($prtype_r) {  echo $prtype_r;  }?></td>
		<td><?php if($l_type_l_) {  echo $l_type_l_;  }?></td>
      </tr>
      
    </table>
  </div>
  <div class="column">
    <table>
	<h4>LEFT</h4>
      <tr>
        <th>Sph</th>
        <th>Cyl</th>
        <th>Axis</th>
		<th>Prism</th>
		<th>P.Type</th>
		<th>L.Type</th>
		<th>Adtn</th>
		<th>Nr./Far</th>
	
      </tr>
      <tr>
      
        <td><?php if($sph_l_) {  echo $sph_l_;  }?></td>
		<td><?php if($cyl_l) {  echo $cyl_l;  }?></td>
		<td><?php if($axis_l) {  echo $axis_l;  }?></td>
		<td><?php if($prism_l) {  echo $prism_l;  }?></td>	
		<td><?php if($prtype_l) {  echo $prtype_l;  }?></td>
		<td><?php if($l_type_l_) {  echo $l_type_l_;  }?></td>
		<td><?php if($cl_adtn) {  echo $cl_adtn;  }?></td>
		<td><?php if($near_far_) {  echo $near_far_;  }?></td>
      </tr>
      
    </table>
  </div>
</div>

<h2>2.CONTACT(Prescription History)</h2>


<div class="row">
  <div class="column">
    <table>
	<h4>RIGHT</h4>
      <tr>
        <th>Date</th>
        <th>OrderNO.</th>
        <th>Sph</th>
		<th>Cyl</th>
		<th>Axis</th>
		<th>Dia</th>
		<th>BCurv</th>
		<th>CL.Type</th>
		
		
      </tr>
      <tr>
        <td><?php if($date) {  echo $date;  }?></td>
        <td><?php if($inv_no_b_) {  echo $inv_no_b_;  }?></td>
        <td><?php if($sph_r_) {  echo $sph_r_;  }?></td>
		<td><?php if($cyl_r) {  echo $cyl_r;  }?></td>
		<td><?php if($axis_r) {  echo $axis_r;  }?></td>
		<td><?php if($cl_dia_r_) {  echo $prism_r_;  }?></td>	
		<td><?php if($cl_bcurv_r) {  echo $prtype_r;  }?></td>
		<td><?php if($cl_type_r_) {  echo $l_type_l_;  }?></td>
      </tr>
     
    </table>
  </div>
  <div class="column">
    <table>
	<h4>LEFT</h4>
      <tr>
        <th>Sph</th>
        <th>Cyl</th>
        <th>Axis</th>
		<th>Dia</th>
		<th>bCurv</th>
		<th>CL.Type</th>
		<th>Adtn</th>
		<th>Nr./Far</th>
	
      </tr>
      <tr>
      
        <td><?php if($sph_l_) {  echo $sph_l_;  }?></td>
		<td><?php if($cyl_l) {  echo $cyl_l;  }?></td>
		<td><?php if($axis_l) {  echo $axis_l;  }?></td>
		<td><?php if($cl_dia_l) {  echo $prism_l;  }?></td>	
		<td><?php if($cl_bcurv_l) {  echo $prtype_l;  }?></td>
		<td><?php if($cl_type_l_) {  echo $l_type_l_;  }?></td>
		<td><?php if($cl_adtn) {  echo $cl_adtn;  }?></td>
		<td><?php if($near_far_) {  echo $near_far_;  }?></td>
      </tr>
     
    </table>
  </div>
</div>

<h2>3.FRAME SUNGLASS AND OTHERS (Sales)</h2>


<div class="row">
  <div class="column">
    <table>
	
      <tr>
        <th>Date</th>
        <th>OrderNO.</th>
        <th>Code</th>
		<th>Brand</th>
		<th>Material</th>
		<th>Color</th>
		<th>Size</th>
		<th>Model</th>
		<th>Description</th>
		<th>Qty</th>
		
		
      </tr>
      <tr>
        <td><?php if($date) {  echo $date;  }?></td>
        <td><?php if($inv_no_b_) {  echo $inv_no_b_;  }?></td>
        <td><?php if($code) {  echo $code;  }?></td>
		<td><?php if($brand) {  echo $brand;  }?></td>
		<td><?php if($material) {  echo $material;  }?></td>
		<td><?php if($color) {  echo $color;  }?></td>	
		<td><?php if($size) {  echo $size;  }?></td>
		<td><?php if($model) {  echo $model;  }?></td>
		<td><?php if($descript) {  echo $descript;  }?></td>
		<td><?php if($cl_qty_l) {  echo $cl_qty_l;  }?></td>
      </tr>
      
    </table>
  </div>
</div>

		 	<?php	
		 	//Test html
		 
          	} // while loop ends
		}
		// update_option( "finalresults_title", $results );	
		// update_option( "accno", $accno );	
		// update_option( "customer_name", $customer_name );	
		// update_option( "add", $add );	
		// update_option( "add2", $add2 );	
		// update_option( "invno", $invno );	
		// update_option( "date", $date );	
		// update_option( "deldate", $deldate );	
		// update_option( "cur_date", $cur_date );	
		// update_option( "fromacc", $fromacc );	
		// update_option( "stktr", $stktr );	
		// update_option( "sph_r_", $sph_r_ );	
		// update_option( "sph_r_2_", $sph_r_2_ );	
		// update_option( "sph_r_3", $sph_r_3 );	
		// update_option( "sph_l_", $sph_l_ );	
		// update_option( "sph_l_2", $sph_l_2 );	
		// update_option( "sph_l_3_", $sph_l_3_ );	
		// update_option( "cyl_r", $cyl_r );	
		// update_option( "cyl_r_2_", $cyl_r_2_ );	
		// update_option( "cyl_r_3", $cyl_r_3 );	
		// update_option( "cyl_l", $cyl_l );	
		// update_option( "cyl_l_2", $cyl_l_2 );	
		// update_option( "cyl_l_3_", $cyl_l_3_ );	
		// update_option( "axis_r", $axis_r );	
		// update_option( "axis_r_2_", $axis_r_2_ );	
		// update_option( "axis_r_3", $axis_r_3 );	
		// update_option( "axis_l", $axis_l );	
		// update_option( "axis_l_2", $axis_l_2 );	
		// update_option( "axis_l_3", $axis_l_3 );	
		// update_option( "prism_r_", $prism_r_ );	
		// update_option( "prism_l", $prism_l );	
		// update_option( "prtype_l", $prtype_l );	
		// update_option( "prtype_r", $prtype_r );	
		// update_option( "prtype_r2", $prtype_r2 );	
		// update_option( "prtype_r3", $prtype_r3 );	
		// update_option( "addition", $addition );	
		// update_option( "addition2", $addition2 );	
		// update_option( "addition3_", $addition3_ );	
		// update_option( "right_", $right_ );	
		// update_option( "left_", $left_ );	
		// update_option( "near_far_", $near_far_ );	
		// update_option( "near_far2_", $near_far2_ );	
		// update_option( "near_far3", $near_far3 );	
		// update_option( "n_f_bi", $n_f_bi );	
		// update_option( "n_f_bi2_", $n_f_bi2_ );	
		// update_option( "n_f_bi3", $n_f_bi3 );	
		// update_option( "cl_nfbi", $cl_nfbi );	
		// update_option( "l_type_r_", $l_type_r_ );	
		// update_option( "l_type_r2", $l_type_r2 );	
		// update_option( "l_type_r3", $l_type_r3 );	
		// update_option( "l_type_l_", $l_type_l_ );	
		// update_option( "l_type_l2_", $l_type_l2_ );	
		// update_option( "l_type_l3", $l_type_l3 );	
		// update_option( "l_rate", $l_rate );	
		// update_option( "l_link_r_", $l_link_r_ );	
		// update_option( "l_link_r2", $l_link_r2 );	
		// update_option( "l_link_r3_", $l_link_r3_ );	
		// update_option( "l_link_l_", $l_link_l_ );	
		// update_option( "l_link_l2", $l_link_l2 );	
		// update_option( "l_link_l3", $l_link_l3 );	
		// update_option( "cl_link_r_", $cl_link_r_ );	
		// update_option( "cl_link_l_", $cl_link_l_ );	
		// update_option( "l_qty_r", $l_qty_r );	
		// update_option( "l_qty_r2", $l_qty_r2 );	
		// update_option( "l_qty_r3", $l_qty_r3 );	
		// update_option( "l_qty_l_", $l_qty_l_ );	
		// update_option( "l_qty_l2", $l_qty_l2 );	
		// update_option( "l_qty_l3_", $l_qty_l3_ );	
		// update_option( "lamt1", $lamt1 );	
		// update_option( "lamt2", $lamt2 );	
		// update_option( "lamt3", $lamt3 );	
		// update_option( "clamt_", $clamt_ );	
		// update_option( "cl_sph_r", $cl_sph_r );	
		// update_option( "cl_sph_l_", $cl_sph_l_ );	
		// update_option( "cl_cyl_r_", $cl_cyl_r_ );	
		// update_option( "cl_cyl_l_", $cl_cyl_l_ );	
		// update_option( "cl_axis_r_", $cl_axis_r_ );	
		// update_option( "cl_axis_l", $cl_axis_l );	
		// update_option( "cl_type_r_", $cl_type_r_ );	
		// update_option( "cl_type_l_", $cl_type_l_ );	
		// update_option( "cl_adtn", $cl_adtn );	
		// update_option( "cl_dia_r_", $cl_dia_r_ );	
		// update_option( "cl_dia_l", $cl_dia_l );	
		// update_option( "cl_bcurv_r", $cl_bcurv_r );	
		// update_option( "cl_bcurv_l", $cl_bcurv_l );	
		// update_option( "cl_qty_r_", $cl_qty_r_ );	
		// update_option( "cl_qty_l", $cl_qty_l );	
		// update_option( "inv_no_b_", $inv_no_b_ );	
		// update_option( "descript", $descript );	
		// update_option( "brand", $brand );	
		// update_option( "model", $model );	
		// update_option( "color_", $color_ );	
		// update_option( "size", $size );	
		// update_option( "material", $material );	
		// update_option( "code", $code );	
	}

	ob_start();
	//$page_title = get_the_title();
	//$terms = get_the_terms( get_the_ID(), 'product_cat' );
	//$product_type = $terms[0]->slug;
	?>
	<form role="form" action="" id="submit_prod" enctype="multipart/form-data" method="post">						
		<?php 
			//Check whether it is home page
			//$home = get_post_meta($product_id, 'home_check');
			$finalresults_title = get_option( "finalresults_title");
			$accno = get_option( "accno");
			$customer_name = get_option( "customer_name");
			$add = get_option( "add");
			$add2 = get_option( "add2");
			$invno = get_option( "invno");
			$date = get_option( "date");
			$deldate = get_option( "deldate");
			$cur_date = get_option( "cur_date");
			$fromacc = get_option( "fromacc");
			$stktr = get_option( "stktr");
			$sph_r_ = get_option( "sph_r_");
			$sph_r_2_ = get_option( "sph_r_2_");
			$sph_r_3 = get_option( "sph_r_3");
			$sph_l_ = get_option( "sph_l_");
			$sph_l_2= get_option( "sph_l_2");
			$sph_l_3_ = get_option( "sph_l_3_");
			$cyl_r = get_option( "cyl_r");
			$cyl_r_2_ = get_option( "cyl_r_2_");
			$cyl_r_3 = get_option( "cyl_r_3");
			$cyl_l = get_option( "cyl_l");
			$cyl_l_2 = get_option( "cyl_l_2");
			$cyl_l_3_ = get_option( "cyl_l_3_");
			$axis_r = get_option( "axis_r");
			$axis_r_2_ = get_option( "axis_r_2_");
			$axis_r_3 = get_option( "axis_r_3");
			$axis_l = get_option( "axis_l");
			$axis_l_2 = get_option( "axis_l_2");
			$axis_l_3 = get_option( "axis_l_3");
			$prism_r_ = get_option( "prism_r_");
			$prism_l = get_option( "prism_l");
			$prtype_l = get_option( "prtype_l");
			$prtype_r = get_option( "prtype_r");
			$prtype_r2 = get_option( "prtype_r2");
			$prtype_r3 = get_option( "prtype_r3");
			$addition = get_option( "addition");
			$addition2 = get_option( "addition2");
			$addition3_ = get_option( "addition3_");
			$right_ = get_option( "right_");
			$left_ = get_option( "left_");
			$near_far_ = get_option( "near_far_");
			$near_far2_ = get_option( "near_far2_");
			$near_far3 = get_option( "near_far3");
			$n_f_bi = get_option( "n_f_bi");
			$n_f_bi2_ = get_option( "n_f_bi2_");
			$n_f_bi3 = get_option( "n_f_bi3");
			$cl_nfbi = get_option( "cl_nfbi");
			$l_type_r_ = get_option( "l_type_r_");
			$l_type_r2 = get_option( "l_type_r2");
			$l_type_r3 = get_option( "l_type_r3");
			$l_type_l_ = get_option( "l_type_l_");
			$l_type_l2_ = get_option( "l_type_l2_");
			$l_type_l3 = get_option( "l_type_l3");
			$l_rate = get_option( "l_rate");
			$l_link_r_ = get_option( "l_link_r_");
			$l_link_r2 = get_option( "l_link_r2");
			$l_link_r3_ = get_option( "l_link_r3_");
			$l_link_l_ = get_option( "l_link_l_");
			$l_link_l2 = get_option( "l_link_l2");
			$l_link_l3 = get_option( "l_link_l3");
			$cl_link_r_ = get_option( "cl_link_r_");
			$cl_link_l_ = get_option( "cl_link_l_");
			$l_qty_r = get_option( "l_qty_r");
			$l_qty_r2 = get_option( "l_qty_r2");
			$l_qty_r3 = get_option( "l_qty_r3");
			$l_qty_l_ = get_option( "l_qty_l_");
			$l_qty_l2 = get_option( "l_qty_l2");
			$l_qty_l3_ = get_option( "l_qty_l3_");
			$lamt1 = get_option( "lamt1");
			$lamt2 = get_option( "lamt2");
			$lamt3 = get_option( "lamt3");
			$clamt_ = get_option( "clamt_");
			$cl_sph_r = get_option( "cl_sph_r");
			$cl_sph_l_ = get_option( "cl_sph_l_");
			$cl_cyl_r_ = get_option( "cl_cyl_r_");
			$cl_cyl_l_ = get_option( "cl_cyl_l_");
			$cl_axis_r_ = get_option( "cl_axis_r_");
			$cl_axis_l = get_option( "cl_axis_l");
			$cl_type_r_ = get_option( "cl_type_r_");
			$cl_type_l_ = get_option( "cl_type_l_");
			$cl_adtn = get_option( "cl_adtn");
			$cl_dia_r_ = get_option( "cl_dia_r_");
			$cl_dia_l = get_option( "cl_dia_l");
			$cl_bcurv_r = get_option( "cl_bcurv_r");
			$cl_bcurv_l = get_option( "cl_bcurv_l");
			$cl_qty_r_ = get_option( "cl_qty_r_");
			$cl_qty_l = get_option( "cl_qty_l");
			$inv_no_b_ = get_option( "inv_no_b_");
			$descript = get_option( "descript");
			$brand = get_option( "brand");
			$model = get_option( "model");
			$color_ = get_option( "color_");
			$size = get_option( "size");
			$material = get_option( "material");
			$code = get_option( "code");
			
			if($finalresults_title[0]){
				//echo $finalresults_title[0];
				echo "<br>";
			}
			
			?>
			<!-- HTML Start-->
                 <style>
                         * {
                              box-sizing: border-box;
                                  }

                      .row {
                              display: flex;
                               margin-left:-5px;
                              margin-right:-5px;
                            }



                      table {
                           border-collapse: collapse;
                            border-spacing: 0;
                              width: 100%;
                          border: 1px solid #ddd;
                            }

                      th, td {
                                text-align: left;
                              padding: 16px;
                              }

              tr:nth-child(even) {
                                     background-color: #f2f2f2;
                                  }
             </style>
            
            <!--   
			   <table style="width:100%">

                     <?php if($add) {?>
					 <tr>
                       <th>Address</th>
                        <td>
							<?php echo $add;?>
						</td>
                     </tr>
					 <?php }?>
					 
					  <?php if($accno) {?>
					  <tr>
                       <th>Account Number</th>
                        <td>
							<?php echo $accno;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 <?php if($customer_name) {?>
					  <tr>
                       <th>Customer Name</th>
                        <td>
							<?php echo $customer_name;?>
						</td>
                     </tr>
					 <?php }?>
					 
					
                     
					 <?php if($add) {?>
					   <tr>
                       <th>Address</th>
                        <td>
							<?php echo $add;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($add2) {?>
					   <tr>
                       <th>Number</th>
                        <td>
							<?php echo $add2;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($invno) {?>
					   <tr>
                       <th>Invoice Number</th>
                        <td>
							<?php echo $invno;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($date) {?>
					   <tr>
                       <th>Date</th>
                        <td>
							<?php echo $date;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($deldate) {?>
					   <tr>
                       <th>Delay Date</th>
                        <td>
							<?php echo $deldate;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 <?php if($cur_date) {?>
					   <tr>
                       <th>Current Date</th>
                        <td>
							<?php echo $cur_date;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 
					 
					 <?php if($code) {?>
					   <tr>
                       <th>Code</th>
                        <td>
							<?php echo $code;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($model) {?>
					   <tr>
                       <th>Model</th>
                        <td>
							<?php echo $model;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($color_) {?>
					   <tr>
                       <th>Color</th>
                        <td>
							<?php echo $color_;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 <?php if($size) {?>
					   <tr>
                       <th>Size</th>
                        <td>
							<?php echo $size;?>
						</td>
                     </tr>
					 <?php }?> 
					 
					 
					 <?php if($material) {?>
					   <tr>
                       <th>Material</th>
                        <td>
							<?php echo $material;?>
						</td>
                     </tr>
					 <?php }?>

			
					 
					 <?php if($fromacc) {?>
					   <tr>
                       <th>fromacc</th>
                        <td>
							<?php echo $fromacc;?>
						</td>
                     </tr>
					 <?php }?>
			
					 
					 <?php if($stktr) {?>
					   <tr>
                       <th>stktr</th>
                        <td>
							<?php echo $stktr;?>
						</td>
                     </tr>
					 <?php }?>

			 
					 <?php if($sph_r_) {?>
					   <tr>
                       <th>sph_r_</th>
                        <td>
							<?php echo $sph_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
			 
					 <?php if($sph_r_2_) {?>
					   <tr>
                       <th>sph_r_2_</th>
                        <td>
							<?php echo $sph_r_2_;?>
						</td>
                     </tr>
					 <?php }?>

			
			 
					 <?php if($sph_r_3) {?>
					   <tr>
                       <th>sph_r_3</th>
                        <td>
							<?php echo $sph_r_3;?>
						</td>
                     </tr>
					 <?php }?>

			
			
			 
					 <?php if($sph_l_) {?>
					   <tr>
                       <th>sph_l_</th>
                        <td>
							<?php echo $sph_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
			
			 
					 <?php if($sph_l_2) {?>
					   <tr>
                       <th>sph_l_2</th>
                        <td>
							<?php echo $sph_l_2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($sph_r_) {?>
					   <tr>
                       <th>sph_r_</th>
                        <td>
							<?php echo $sph_r_;?>
						</td>
                     </tr>
					 <?php }?> 
					
			
			 
					 <?php if($sph_r_2_) {?>
					   <tr>
                       <th>sph_r_2_</th>
                        <td>
							<?php echo $sph_r_2_;?>
						</td>
                     </tr>
					 <?php }?> 
					 
					 
					 
					 <?php if($sph_r_3) {?>
					   <tr>
                       <th>sph_r_3</th>
                        <td>
							<?php echo $sph_r_3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($sph_l_) {?>
					   <tr>
                       <th>sph_l_</th>
                        <td>
							<?php echo $sph_l_;?>
						</td>
                     </tr>
					 <?php }?>

					
			
			 
					 <?php if($sph_l_2) {?>
					   <tr>
                       <th>sph_l_2</th>
                        <td>
							<?php echo $sph_l_2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($sph_l_3_) {?>
					   <tr>
                       <th>sph_l_3_</th>
                        <td>
							<?php echo $sph_l_3_;?>
						</td>
                     </tr>
					 <?php }?>

					
			
			 
					 <?php if($cyl_r) {?>
					   <tr>
                       <th>cyl_r</th>
                        <td>
							<?php echo $cyl_r;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($cyl_r_2_) {?>
					   <tr>
                       <th>cyl_r_2_</th>
                        <td>
							<?php echo $cyl_r_2_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
					
			
			 
					 <?php if($cyl_r_3) {?>
					   <tr>
                       <th>cyl_r_3</th>
                        <td>
							<?php echo $cyl_r_3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
			 
					 <?php if($cyl_l) {?>
					   <tr>
                       <th>cyl_l</th>
                        <td>
							<?php echo $cyl_l;?>
						</td>
                     </tr>
					 <?php }?>

					
			
			 
					 <?php if($cyl_l_2) {?>
					   <tr>
                       <th>cyl_l_2</th>
                        <td>
							<?php echo $cyl_l_2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
			 
					 <?php if($cyl_l_3_) {?>
					   <tr>
                       <th>cyl_l_3_</th>
                        <td>
							<?php echo $cyl_l_3_;?>
						</td>
                     </tr>
					 <?php }?>

                   
			 
					 <?php if($axis_r) {?>
					   <tr>
                       <th>axis_r</th>
                        <td>
							<?php echo $axis_r;?>
						</td>
                     </tr>
					 <?php }?>

                   
			 
					 <?php if($axis_r_2_) {?>
					   <tr>
                       <th>axis_r_2_</th>
                        <td>
							<?php echo $axis_r_2_;?>
						</td>
                     </tr>
					 <?php }?>

			
			 
					 <?php if($axis_r_3) {?>
					   <tr>
                       <th>axis_r_3</th>
                        <td>
							<?php echo $axis_r_3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			 
					 <?php if($axis_l) {?>
					   <tr>
                       <th>axis_l</th>
                        <td>
							<?php echo $axis_l;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			 
					 <?php if($axis_l_2) {?>
					   <tr>
                       <th>axis_l_2</th>
                        <td>
							<?php echo $axis_l_2;?>
						</td>
                     </tr>
					 <?php }?>

					
			 
					 <?php if($axis_l_3) {?>
					   <tr>
                       <th>axis_l_3</th>
                        <td>
							<?php echo $axis_l_3;?>
						</td>
                     </tr>
					 <?php }?>
					
			 
					 <?php if($prism_r_) {?>
					   <tr>
                       <th>prism_r_</th>
                        <td>
							<?php echo $prism_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 
					 <?php if($prism_l) {?>
					   <tr>
                       <th>prism_l</th>
                        <td>
							<?php echo $prism_l;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					  
					 <?php if($prtype_l) {?>
					   <tr>
                       <th>prtype_l</th>
                        <td>
							<?php echo $prtype_l;?>
						</td>
                     </tr>
					 <?php }?>
                   
					  
					 <?php if($prtype_r) {?>
					   <tr>
                       <th>prtype_r</th>
                        <td>
							<?php echo $prtype_r;?>
						</td>
                     </tr>
					 <?php }?>

					  
					 <?php if($prtype_r2) {?>
					   <tr>
                       <th>prtype_r2</th>
                        <td>
							<?php echo $prtype_r2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($prtype_r3) {?>
					   <tr>
                       <th>prtype_r3</th>
                        <td>
							<?php echo $prtype_r3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($addition) {?>
					   <tr>
                       <th>addition</th>
                        <td>
							<?php echo $addition;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($addition2) {?>
					   <tr>
                       <th>addition2</th>
                        <td>
							<?php echo $addition2;?>
						</td>
                     </tr>
					 <?php }?>
					
                   
					 <?php if($addition3_) {?>
					   <tr>
                       <th>addition3_</th>
                        <td>
							<?php echo $addition3_;?>
						</td>
                     </tr>
					 <?php }?>

			
                   
					 <?php if($right_) {?>
					   <tr>
                       <th>right_</th>
                        <td>
							<?php echo $right_;?>
						</td>
                     </tr>
					 <?php }?>

			
                   
					 <?php if($left_) {?>
					   <tr>
                       <th>left_</th>
                        <td>
							<?php echo $left_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
                   
					 <?php if($near_far_) {?>
					   <tr>
                       <th>near_far_</th>
                        <td>
							<?php echo $near_far_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($near_far2_) {?>
					   <tr>
                       <th>near_far2_</th>
                        <td>
							<?php echo $near_far2_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
			
					
					 <?php if($near_far3) {?>
					   <tr>
                       <th>near_far3</th>
                        <td>
							<?php echo $near_far3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
                   
			
					
					 <?php if($n_f_bi) {?>
					   <tr>
                       <th>n_f_bi</th>
                        <td>
							<?php echo $n_f_bi;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
			
					
					 <?php if($n_f_bi2_) {?>
					   <tr>
                       <th>n_f_bi2_</th>
                        <td>
							<?php echo $n_f_bi2_;?>
						</td>
                     </tr>
					 <?php }?>

                   
			
					
					 <?php if($n_f_bi3) {?>
					   <tr>
                       <th>n_f_bi3</th>
                        <td>
							<?php echo $n_f_bi3;?>
						</td>
                     </tr>
					 <?php }?>


					 <?php if($cl_nfbi) {?>
					   <tr>
                       <th>cl_nfbi</th>
                        <td>
							<?php echo $cl_nfbi;?>
						</td>
                     </tr>
					 <?php }?>
			
					
                   
			
					
					 <?php if($l_type_r_) {?>
					   <tr>
                       <th>l_type_r_</th>
                        <td>
							<?php echo $l_type_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($l_type_r2) {?>
					   <tr>
                       <th>l_type_r2</th>
                        <td>
							<?php echo $l_type_r2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($l_type_r3) {?>
					   <tr>
                       <th>l_type_r3</th>
                        <td>
							<?php echo $l_type_r3;?>
						</td>
                     </tr>
					 <?php }?>

			
					

			
					
                   
					 
					
					 <?php if($l_type_l_) {?>
					   <tr>
                       <th>l_type_l_</th>
                        <td>
							<?php echo $l_type_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
			
					
                   
					 
					
					 <?php if($l_type_l2_) {?>
					   <tr>
                       <th>l_type_l2_</th>
                        <td>
							<?php echo $l_type_l2_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 
					
					 <?php if($l_type_l3) {?>
					   <tr>
                       <th>l_type_l3</th>
                        <td>
							<?php echo $l_type_l3;?>
						</td>
                     </tr>
					 <?php }?>
					 
					
					 <?php if($l_rate) {?>
					   <tr>
                       <th>l_rate</th>
                        <td>
							<?php echo $l_rate;?>
						</td>
                     </tr>
					 <?php }?>
					
					 <?php if($l_link_r_) {?>
					   <tr>
                       <th>l_link_r_</th>
                        <td>
							<?php echo $l_link_r_;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($l_link_r2) {?>
					   <tr>
                       <th>l_link_r2</th>
                        <td>
							<?php echo $l_link_r2;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($l_link_r3_) {?>
					   <tr>
                       <th>l_link_r3_</th>
                        <td>
							<?php echo $l_link_r3_;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($l_link_l_) {?>
					   <tr>
                       <th>l_link_l_</th>
                        <td>
							<?php echo $l_link_l_;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($l_link_l2) {?>
					   <tr>
                       <th>l_link_l2</th>
                        <td>
							<?php echo $l_link_l2;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($l_link_l3) {?>
					   <tr>
                       <th>l_link_l3</th>
                        <td>
							<?php echo $l_link_l3;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($cl_link_r_) {?>
					   <tr>
                       <th>cl_link_r_</th>
                        <td>
							<?php echo $cl_link_r_;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($cl_link_l_) {?>
					   <tr>
                       <th>cl_link_l_</th>
                        <td>
							<?php echo $cl_link_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($l_qty_r) {?>
					   <tr>
                       <th>l_qty_r</th>
                        <td>
							<?php echo $l_qty_r;?>
						</td>
                     </tr>
					 <?php }?>
					
					 <?php if($l_qty_r2) {?>
					   <tr>
                       <th>l_qty_r2</th>
                        <td>
							<?php echo $l_qty_r2;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($l_qty_r3) {?>
					   <tr>
                       <th>l_qty_r3</th>
                        <td>
							<?php echo $l_qty_r3;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 
					
					 <?php if($l_qty_l_) {?>
					   <tr>
                       <th>l_qty_l_</th>
                        <td>
							<?php echo $l_qty_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					
					 <?php if($l_qty_l2) {?>
					   <tr>
                       <th>l_qty_l2</th>
                        <td>
							<?php echo $l_qty_l2;?>
						</td>
                     </tr>
					 <?php }?>
                   
					
					 <?php if($l_qty_l3_) {?>
					   <tr>
                       <th>l_qty_l3_</th>
                        <td>
							<?php echo $l_qty_l3_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($lamt1) {?>
					   <tr>
                       <th>lamt1</th>
                        <td>
							<?php echo $lamt1;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($lamt2) {?>
					   <tr>
                       <th>lamt2</th>
                        <td>
							<?php echo $lamt2;?>
						</td>
                     </tr>
					 <?php }?>

			
					 <?php if($lamt3) {?>
					   <tr>
                       <th>lamt3</th>
                        <td>
							<?php echo $lamt3;?>
						</td>
                     </tr>
					 <?php }?>

			
					 <?php if($clamt_) {?>
					   <tr>
                       <th>clamt_</th>
                        <td>
							<?php echo $clamt_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($cl_sph_r) {?>
					   <tr>
                       <th>cl_sph_r</th>
                        <td>
							<?php echo $cl_sph_r;?>
						</td>
                     </tr>
					 <?php }?>
	
                   
					 <?php if($cl_sph_l_) {?>
					   <tr>
                       <th>cl_sph_l_</th>
                        <td>
							<?php echo $cl_sph_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					 <?php if($cl_cyl_r_) {?>
					   <tr>
                       <th>cl_cyl_r_</th>
                        <td>
							<?php echo $cl_cyl_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
			
					 <?php if($cl_cyl_l_) {?>
					   <tr>
                       <th>cl_cyl_l_</th>
                        <td>
							<?php echo $cl_cyl_l_;?>
						</td>
                     </tr>
					 <?php }?>

			
					 <?php if($cl_axis_r_) {?>
					   <tr>
                       <th>cl_axis_r_</th>
                        <td>
							<?php echo $cl_axis_r_;?>
						</td>
                     </tr>
					 <?php }?>
			
					 <?php if($cl_axis_l) {?>
					   <tr>
                       <th>cl_axis_l</th>
                        <td>
							<?php echo $cl_axis_l;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 <?php if($cl_type_r_) {?>
					   <tr>
                       <th>cl_type_r_</th>
                        <td>
							<?php echo $cl_type_r_;?>
						</td>
                     </tr>
					 <?php }?>

                   
					 <?php if($cl_type_l_) {?>
					   <tr>
                       <th>cl_type_l_</th>
                        <td>
							<?php echo $cl_type_l_;?>
						</td>
                     </tr>
					 <?php }?>

                   
					 <?php if($cl_adtn) {?>
					   <tr>
                       <th>cl_adtn</th>
                        <td>
							<?php echo $cl_adtn;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 <?php if($cl_dia_r_) {?>
					   <tr>
                       <th>cl_dia_r_</th>
                        <td>
							<?php echo $cl_dia_r_;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($cl_dia_l) {?>
					   <tr>
                       <th>cl_dia_l</th>
                        <td>
							<?php echo $cl_dia_l;?>
						</td>
                     </tr>
					 <?php }?>

					 <?php if($cl_bcurv_r) {?>
					   <tr>
                       <th>cl_bcurv_r</th>
                        <td>
							<?php echo $cl_bcurv_r;?>
						</td>
                     </tr>
					 <?php }?>
					 
					 
					 <?php if($cl_bcurv_l) {?>
					   <tr>
                       <th>cl_bcurv_l</th>
                        <td>
							<?php echo $cl_bcurv_l;?>
						</td>
                     </tr>
					 <?php }?>

					 
					 <?php if($cl_qty_r_) {?>
					   <tr>
                       <th>cl_qty_r_</th>
                        <td>
							<?php echo $cl_qty_r_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 
					 <?php if($cl_qty_l) {?>
					   <tr>
                       <th>cl_qty_l</th>
                        <td>
							<?php echo $cl_qty_l;?>
						</td>
                     </tr>
					 <?php }?>

					
					 
					 <?php if($inv_no_b_) {?>
					   <tr>
                       <th>inv_no_b_</th>
                        <td>
							<?php echo $inv_no_b_;?>
						</td>
                     </tr>
					 <?php }?>

			
					
					 
					 <?php if($descript) {?>
					   <tr>
                       <th>descript</th>
                        <td>
							<?php echo $descript;?>
						</td>
                     </tr>
					 <?php }?>

			
					
                   
					 
					
               </table>
 			-->


<!-- HTML Ends-->
			
			
			<?php
		?>	
		<div id="choptoolmain" class="custom_calculator single_product">
		  	<div class="toolleftparent fusion-fullwidth fullwidth-box fusion-builder-row-9 ">
			  	<div class="row">
					<div class="toolsidebars toolleft col medium-2 col-3 col-sm-12 col-md-3 col-lg-2">
						<h3>Customer Login</h3>						
						<div class="plateselect">
							<input type="text" id="phone_num" name="phone_num">
							<span id="phone_validate" style="display: none;">Valid</span>
							<span id="phone_invalid" style="display: none;">Invalid</span>
						</div>				
				    </div>
			    </div>
		    </div>
		    <div class="container palfooter">
			  	<div class="row">
					<div class="col medium-4 col-4 col-sm-2 col-md-4 col-lg-4"></div>			
					<div class="col medium-4 col-4 col-sm-12 col-md-4 col-lg-4 resetbutton">
						<button type="submit" id="save_size" name="save_size" class="btn btn-primary">Login</button>
					</div>			
					<div class="col medium-4 col-4 col-sm-2 col-md-4 col-lg-4"></div>			
				</div>			
			</div>
		</div>

	</form>			
	<?php 
	
	$output = ob_get_contents(); //Gives whatever has been "saved"
	ob_end_clean(); //Stops saving things and discards whatever was saved
	ob_flush(); //Stops saving and outputs it all at once
	return $output;
}

function ajax_enqueue() {
    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/my-ajax-script.js', array('jquery') );
    wp_localize_script( 'ajax-script', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'ajax_enqueue' );
add_action( 'wp_ajax_foobar', 'wp_ajax_foobar_handler' );
add_action( 'wp_ajax_nopriv_foobar', 'wp_ajax_foobar_handler' );

function wp_ajax_foobar_handler() {

		// //$h = $_REQUEST['lb_cropperh'];
		// $phone_num = $_REQUEST['phone_num'];
	 //    $jpeg_quality = 90;

	 //    $yourPostTitle = $phone_num;
		// $yourPostTitle =strtoupper($yourPostTitle);
		// $ids = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE UCASE(post_title) LIKE '%$yourPostTitle%' AND post_type='spot' AND post_status='publish'");
		// if ($ids) {
		//   $args=array(
		//     'post__in' => $ids,
		//     'posts_per_page' => -1,
		//     'caller_get_posts'=> 1
		//   );
		//   $my_query = null;
		//   $my_query = new WP_Query($args);
		//   if( $my_query->have_posts() ) {
		//     echo 'List of Posts';
		//     while ($my_query->have_posts()) : $my_query->the_post(); ?>
		//       <!--<p><a href="<?php //the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php //the_title_attribute(); ?>"><?php //the_title(); ?></a></p>-->
		//       <?php
		//     endwhile;
		//   }
		// wp_reset_query();  // Restore global post data stomped by the_post().
		// }
	 //    //echo "<input id='cropped_url' type='text' value='".$attachment_url_option."'>";
	 //    //update_option( 'cropped_image_url', $attachment_url );
	 //    wp_die();
}